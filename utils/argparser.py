from argparse import Action


def join(args: list) -> str:
    def split(args: list):
        try:
            index = args.index('\n')
            split_by_lines.append(args[:index])
            split_by_lines.append(['\n'])
            split(args[index + 1:])
        except ValueError:
            split_by_lines.append(args)

    split_by_lines = []
    split(args)

    out = ''
    for args in split_by_lines:
        out = out + ' '.join(args)
    return out


class StoreSingleLine(Action):
    # {'nargs': '+', 'default': str(), 'action': StoreSingleLine}
    def __init__(self, *args, **kwargs):
        super(StoreSingleLine, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, ' '.join(values).strip())


class StoreMultiLine(Action):
    # {'nargs': '+', 'default': str(), 'action': StoreMultiLine}
    def __init__(self, *args, **kwargs):
        super(StoreMultiLine, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, join(values).strip())


class StoreMultipleMultiLines(Action):
    # {'nargs': '+', 'default': tuple(), 'action': StoreMultipleMultiLines}
    def __init__(self, *args, **kwargs):
        super(StoreMultipleMultiLines, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if not getattr(namespace, self.dest):
            setattr(namespace, self.dest, list())
        getattr(namespace, self.dest).append(join(values).strip())
