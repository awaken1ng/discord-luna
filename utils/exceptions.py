class BotException(BaseException):
    """Base exception"""


class ArgParserError(BotException):
    def __init__(self, message: str):
        self.message = message


class ArgParserHelp(BotException):
    pass


class NoArguments(BotException):
    """Raised if no arguments were provided"""

class NotFound(BotException):
    pass


class NotAllowed(BotException):
    """Raised if user has no required permissions"""


class NoPermissions(BotException):
    """Raised if bot has no required permissions"""


class AlreadyExists(BotException):
    pass


class Success(BotException):
    """Raised upon success"""
