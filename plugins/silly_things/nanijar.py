import re

import discord

from core.plugin import BasePlugin
from utils import create_embed, find

latin_n = r"[NＮℕ𝐍𝑁𝑵𝒩𝓝𝔑𝕹𝖭𝗡𝘕𝙉𝙽Ν𝚴𝛮𝜨𝝢𝞜Ⲛꓠ𐔓𐆎ƝǋǊ№n𝐧𝑛𝒏𝓃𝓷𝔫𝕟𝖓𝗇𝗻𝘯𝙣𝚗ոռɳƞη𝛈𝜂𝜼𝝶𝞰ᵰǌｎɴN̋ͮ̊N͉̉͡ṅ͛͒n̆̐ͯn̵]"
latin_a = r"[AＡ𝐀𝐴𝑨𝒜𝓐𝔄𝔸𝕬𝖠𝗔𝘈𝘼𝙰Α𝚨𝛢𝜜𝝖𝞐АᎪᗅꓮ𖽀𐊠ꜲÆӔꜴ🜇ꜶꜸꜺꜼa⍺ａ𝐚𝑎𝒂𝒶𝓪𝔞𝕒𝖆𝖺𝗮𝘢𝙖𝚊ɑα𝛂𝛼𝜶𝝰𝞪а⍶℀℁ꜳæӕꜵꜷꜹꜻꜽã̻͟aͣͫ͋]"
latin_i = r"[Ii˛⍳ｉⅰℹⅈ𝐢𝑖𝒊𝒾𝓲𝔦𝕚𝖎𝗂𝗶𝘪𝙞𝚒ı𝚤ɪɩιιͺ𝛊𝜄𝜾𝝸𝞲іꙇӏꭵᎥ𑣃⍸ɨᵻᵼＩi̿͑̍iͩ̆͆ᛁi̵]"
latin = rf"{latin_n}\s*{latin_a}+\s*{latin_n}\s*{latin_i}+"


class Plugin(BasePlugin):
    name = 'Nani jar'
    triggers = ('nanijar', 'jar')

    async def on_message(self, message: discord.Message, trigger: str, args: list):
        settings = await self.bot.db.get_guild_settings(message.guild.id)
        if not settings.counters:
            response = create_embed(title='The jar is empty', colour=message.guild.me.colour)
        else:
            user = message.mentions[0] if message.mentions else message.author
            worth = 25
            user_total = user_channel = channel_total = server_total = 0
            for channel_id, values in settings.counters['nani'].items():
                channel = find(lambda ch: ch.id == int(channel_id), message.guild.text_channels)
                if channel:
                    for user_id, count in values.items():
                        if int(user_id) == user.id:
                            user_total += count
                        if channel.id == message.channel.id:
                            if int(user_id) == user.id:
                                user_channel = count
                            channel_total += count
                        server_total += count

            desc = f"{'You have' if user == message.author else user.display_name + ' has'} contributed ¥{user_total * worth} to the Nani Jar\n" \
                   f"{'You have' if user == message.author else user.display_name + ' has'} contributed ¥{user_channel * worth} of this channel's ¥{channel_total * worth} total\n" \
                   f"There is ¥{server_total * worth} in the Nani Jar"
            response = create_embed(
                fields=[('💴 Nani Jar', desc)],
                colour=message.guild.me.colour)
        await message.channel.send(embed=response)

    # Counter
    pattern = re.compile(latin + r"|何|[なナ][にニ]", flags=re.IGNORECASE)

    def scan(self, message: discord.Message) -> int:
        result = self.pattern.findall(message.content)
        if result:
            return len(result)
        else:
            return 0

    async def increment(self, message: discord.Message, by: int):
        author_id = str(message.author.id)
        channel_id = str(message.channel.id)
        settings = await self.bot.db.get_guild_settings(message.guild.id)

        if 'nani' not in settings.counters.keys():
            settings.counters['nani'] = {}
        if channel_id not in settings.counters['nani'].keys():
            settings.counters['nani'][channel_id] = {}
        if author_id not in settings.counters['nani'][channel_id].keys():
            settings.counters['nani'][channel_id][author_id] = 0

        settings.counters['nani'][channel_id][author_id] += by
        settings.counters._modified = True  # FIXME workaround for DictFields
        await settings.commit()

    async def on_any_message(self, message):
        count = self.scan(message)
        if count:
            await self.increment(message, count)

    async def on_message_edit(self, before, after):
        count_before = self.scan(before)
        count_after = self.scan(after)
        if count_before >= count_after:
            return
        await self.increment(after, count_after - count_before)
