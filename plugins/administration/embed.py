from argparse import ArgumentTypeError, Action
from typing import Union

import discord
from discord.embeds import EmptyEmbed
from yarl import URL

import utils
from core.plugin import BasePlugin
from utils.argparser import StoreSingleLine, StoreMultiLine, StoreMultipleMultiLines
from utils.exceptions import ArgParserError


def colour(value: str) -> Union[discord.Colour, int]:
    valid_colour_names = ['blue', 'blurple', 'dark blue', 'dark gold', 'dark green', 'dark grey', 'dark magenta',
                          'dark orange', 'dark purple', 'dark red', 'dark teal', 'darker grey', 'default', 'gold',
                          'green', 'greyple', 'light grey', 'lighter grey', 'magenta', 'orange', 'purple', 'red',
                          'teal']
    value = value.lower().strip()
    if value in valid_colour_names:
        return getattr(discord.Colour, value)()
    elif value.startswith('0x'):
        value = int(value, 16)
    elif value.startswith('0o'):
        value = int(value, 8)
    else:
        value = int(value)
    if value > 16777215:
        raise ArgumentTypeError('Value should be less than or equal to 16777215.')
    return value


def url(value: str) -> str:
    if value.startswith('<') and value.endswith('>'):
        value = value[1:-1]
    URL(value)
    return value


def image_url(value: str) -> str:
    if value.startswith('<') and value.endswith('>'):
        value = value[1:-1]
    name = URL(value).name
    valid_extensions = ['.png', '.jpg', '.gif']
    for ext in valid_extensions:
        if name.endswith(ext):
            return value


def field_index(value: str) -> int:
    value = int(value)
    if value < 1:
        raise ArgumentTypeError('Field index should be positive')
    return value


class StoreFieldTitles(Action):
    def __init__(self, *args, **kwargs):
        super(StoreFieldTitles, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        if not getattr(namespace, self.dest):
            setattr(namespace, self.dest, list())
        if not namespace.field_inline:
            namespace.field_inline = list()
        getattr(namespace, self.dest).append(' '.join(values).strip())
        namespace.field_inline.append(True)  # fields are inline by default


class StoreInlineFlags(Action):
    def __init__(self, *args, **kwargs):
        super(StoreInlineFlags, self).__init__(*args, **kwargs)

    def __call__(self, parser, namespace, value, option_string=None):
        if not getattr(namespace, self.dest):
            setattr(namespace, self.dest, list())
        try:
            getattr(namespace, self.dest)[value - 1] = False
        except IndexError:
            raise ArgParserError('Invalid field index')


class Plugin(BasePlugin):
    name = 'Embed message'
    triggers = ('embed',)
    args = {
        '--title': {'nargs': '+', 'default': EmptyEmbed, 'action': StoreSingleLine},
        '--description': {'nargs': '+', 'default': EmptyEmbed, 'action': StoreMultiLine},
        '--footer': {'nargs': '+', 'default': EmptyEmbed, 'action': StoreSingleLine},
        '--footer_icon': {'type': image_url, 'default': EmptyEmbed},
        '--colour': {'type': colour},
        '--color': {'type': colour, 'help': 'Alias for --colour', 'dest': 'colour'},
        '--url': {'type': url, 'default': EmptyEmbed},
        '--icon': {'type': image_url, 'default': EmptyEmbed},
        '--thumbnail': {'type': image_url},
        '--image': {'type': image_url},
        # set default to tuple() so zip() will not throw an error when fields were not declared
        '--field_title': {'nargs': '+', 'default': tuple(), 'action': StoreFieldTitles},
        '--field_description': {'nargs': '+', 'default': tuple(), 'action': StoreMultipleMultiLines},
        '--field_noninline': {'type': field_index, 'default': tuple(), 'action': StoreInlineFlags, 'dest': 'field_inline'}
    }
    permissions = ('owner', 'administrator')

    async def on_message(self, message: discord.Message, trigger: str, args: list):
        args = self.argparser.parse_args(args)
        fields = list(zip(args.field_title, args.field_description, args.field_inline))
        if not args.title and not args.description and not fields:
            raise ArgParserError('Invalid embed, no title, description or a field')
        response = utils.create_embed(
            title=args.title,
            description=args.description,
            footer=args.footer,
            colour=args.colour,
            url=args.url,
            icon=args.icon,
            thumbnail=args.thumbnail,
            image=args.image,
            footer_icon=args.footer_icon,
            fields=fields)
        await message.channel.send(embed=response)
