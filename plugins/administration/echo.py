import discord

from core.plugin import BasePlugin
from utils.argparser import StoreMultiLine


class Plugin(BasePlugin):
    name = 'Echo'
    description = 'Echoes the message it was called with'
    triggers = ('echo', 'say')
    permissions = ('owner', 'administrator')
    args = {
        'message': {'nargs': '+', 'action': StoreMultiLine}
    }

    async def on_message(self, message: discord.Message, trigger: str, args: list):
        args = self.argparser(args)
        await message.channel.send(args.message)
