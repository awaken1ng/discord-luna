from datetime import timedelta

import arrow
import discord

import utils
from . import ServerPlugin


class Plugin(ServerPlugin):
    name = 'Summer timer'
    triggers = ('summer',)

    async def on_message(self, message: discord.Message, trigger: str, args):
        now = arrow.utcnow()
        start = arrow.get(f'{now.year}-06-21T00:00:00-05:00')
        end = arrow.get(f'{now.year}-09-22T23:59:59-05:00')

        if now < start:
            when = start
            what = 'starts in'
            delta = start - now
        elif now > start:
            delta = end - now
            if delta.total_seconds() > 0:
                when = end
                what = 'ends in'
            else:
                when = start.shift(years=1)
                delta = when - now
                what = "start in"

        delta = timedelta(seconds=round(delta.total_seconds()))
        response = utils.create_embed(title=f'☀ Summer {what} {delta}',
                                      colour=0xFFAA44,
                                      timestamp=when.datetime)
        await message.channel.send(embed=response)

