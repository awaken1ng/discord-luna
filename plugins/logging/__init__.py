import re

from utils import find


async def handle_logging_settings(key, settings, message, args):
    if not args:
        if key not in settings.logging.keys():
            response = f'Logging channel is not set'
        else:
            channel = find(lambda ch: ch.id == settings.logging[key], message.guild.text_channels)
            if channel:
                response = f'Logging is enabled in #{channel.name}'
            else:
                response = f'Channel is set, but unavailable'
    else:
        if args[0] == 'disable':
            if key not in settings.logging.keys():
                response = f'Logging is not enabled'
            else:
                del settings.logging[key]
                settings.logging._modified = True  # FIXME
                await settings.commit()
                response = f'Logging has been disabled'
        else:
            match = re.match(r'<#(\d+)>', args[0])
            if not match:
                response = "Couldn't find a channel in the message"
            else:
                channel_id = int(match.group(1))
                channel = find(lambda ch: ch.id == channel_id, message.guild.text_channels)
                if not channel:
                    response = "Couldn't find the channel in the server"
                else:
                    settings.logging[key] = channel.id
                    settings.logging._modified = True  # FIXME
                    await settings.commit()
                    response = f'Logging has been enabled in #{channel.name}'
    return response
