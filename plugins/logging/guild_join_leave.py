import arrow
import discord

import utils
from core.plugin import BasePlugin
from . import handle_logging_settings


class Plugin(BasePlugin):
    name = 'User movement logging'
    triggers = ('logusermovement', 'logusermove')
    permissions = ('administrator',)

    async def on_message(self, message: discord.Message, trigger: str, args: list):
        settings = await self.bot.db.get_guild_settings(guild_id=message.guild.id)
        response = await handle_logging_settings('join_leave', settings, message, args)
        response_embed = utils.create_embed(title='🚪 User movement',
                                            description=response,
                                            colour=message.guild.me.colour)
        await message.channel.send(embed=response_embed)

    async def on_member_join(self, member: discord.Member):
        settings = await self.bot.db.get_guild_settings(guild_id=member.guild.id)
        if 'join_leave' not in settings.logging.keys():
            return

        log_channel = utils.find(lambda ch: ch.id == settings.logging['join_leave'], member.guild.text_channels)
        if not log_channel:
            return

        response = utils.create_embed(
            title='🚪 User movement',
            thumbnail=member.avatar_url,
            fields=[
                (f"\➡ {'User' if not member.bot else 'Bot'} joined",
                 f"**Name:** {member.name}#{member.discriminator}\n"
                 f"**Mention:** {member.mention}\n"
                 f"**Created at:** {arrow.get(member.created_at).format('YYYY-MM-DD HH:mm')} UTC")
            ],
            footer=f'User ID: {member.id}',
            colour=utils.Colours.success,
            timestamp=arrow.utcnow().datetime
        )
        await log_channel.send(embed=response)

    async def on_member_remove(self, member: discord.Member):
        settings = await self.bot.db.get_guild_settings(guild_id=member.guild.id)
        if 'join_leave' not in settings.logging.keys():
            return

        log_channel = utils.find(lambda ch: ch.id == settings.logging['join_leave'], member.guild.text_channels)
        if not log_channel:
            return

        response = utils.create_embed(
            title='🚪 User movement',
            thumbnail=member.avatar_url,
            fields=[
                (f"\↩ {'User' if not member.bot else 'Bot'} left",
                 f"**Name:** {member.name}#{member.discriminator}\n"
                 f"**Mention:** {member.mention}\n"
                 f"**Created at:** {arrow.get(member.created_at).format('YYYY-MM-DD HH:mm')} UTC\n"
                 f"**Joined at:** {arrow.get(member.joined_at).format('YYYY-MM-DD HH:mm')} UTC")
            ],
            footer=f'User ID: {member.id}',
            colour=utils.Colours.danger,
            timestamp=arrow.utcnow().datetime
        )
        await log_channel.send(embed=response)

