import discord

from core.plugin import BasePlugin


class Plugin(BasePlugin):
    name = 'Set name'
    description = "Sets bot's name"
    triggers = ('setname',)
    args = {'username': {'type': str, 'nargs': '+', 'help': 'Name to set'}}
    permissions = ('owner',)

    async def on_message(self, message: discord.Message, trigger, args):
        args = self.argparser.parse_args(args)
        await self.bot.user.edit(username=' '.join(args.username))
