import discord

import utils
from core.plugin import BasePlugin


class Plugin(BasePlugin):
    name = 'No roles list'
    description = 'List members with no roles'
    triggers = ('noroles',)
    permissions = ('manage_roles', 'kitsunemimi_mods')

    async def on_message(self, message: discord.Message, trigger: str, args: list):
        no_roles = [member
                    for member in message.author.guild.members
                    if len(member.roles) == 1]
        count = len(no_roles)

        if no_roles:
            no_roles = '\n'.join([f'{member.mention} ({member.name}#{member.discriminator})' for member in no_roles])
        else:
            no_roles = 'There are no members with no roles.'

        response = utils.create_embed(
            fields=[('🏷 Members with no roles', no_roles)],
            footer=f'{count} in total',
            colour=utils.Colours.info
        )
        await message.channel.send(embed=response)
