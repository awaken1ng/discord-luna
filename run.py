import argparse
import sys

import yaml

from core.client import Client

if __name__ == '__main__':
    print('Loading the config')
    with open('config.yaml') as file:
        config = yaml.load(file)

    parser = argparse.ArgumentParser()
    parser.add_argument('-profile', help='specify which profile to load')
    args = parser.parse_args()

    if not args.profile:
        def check_and_assign(section: str, key: str, read_from: str = None):
            if key not in config[section].keys():
                print(f'{key} is not defined in {section} section, looking for it in first profile')
                first_key = list(config['Profile'].keys())[0]
                if not read_from:
                    read_from = key
                config[section][key] = config['Profile'][first_key][read_from]
        check_and_assign('Client', 'prefix')
        check_and_assign('Client', 'token')
        check_and_assign('Database', 'name', 'database')
    else:
        if 'Profile' not in config.keys():
            print('Profile section is NOT defined')
            sys.exit(0)
        print(f'Loading profile {args.profile}')
        if args.profile not in config['Profile'].keys():
            print(f'No profile {args.profile}')
            sys.exit(0)
        config['Client']['prefix'] = config['Profile'][args.profile]['prefix']
        config['Client']['token'] = config['Profile'][args.profile]['token']
        config['Database']['name'] = config['Profile'][args.profile]['database']
    config.pop('Profile')

    print('Starting the client')
    client = Client(config)
    client.run(config['Client']['token'])
