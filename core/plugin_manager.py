import importlib
import os


class PluginManager:
    def __init__(self, bot):
        self.bot = bot
        self._logger = bot.log.get_logger('Plugin manager')
        self._logger.info('Initializing plugin manager')

        self.modules = []  # Plugin modules
        self.events = {}  # Dictionary of events to list of plugins that have implemented that event

        # Walk down the plugins folder and load them
        plugins_location = bot.config['Client']['plugins_location']
        for root, folders, files in os.walk(plugins_location):
            for file in files:
                filename, ext = os.path.splitext(file)
                if ext == '.py' and not filename.startswith('__'):
                    try:
                        plugin = importlib.import_module('.'.join((root.replace(os.path.sep, '.'), filename)))
                        # Skip disabled plugins
                        if plugin.Plugin.disabled:
                            continue
                        self.modules.append(plugin)
                        self._logger.info(f'{os.path.join(root, file)} loaded')
                    except AttributeError:
                        self._logger.error(f'Failed to load {os.path.join(root, file)}')

        self.instantiate_plugins()
        self._logger.info('Initialized')

    def instantiate_plugins(self, reload=False):
        self._logger.info('Instantiating plugins')
        for module in self.modules:
            if reload and not module.Plugin.hotreload_friendly:
                continue
            instantiated_plugin = module.Plugin(self.bot)
            # Iterate over implemented events
            implemented_events = [key for key in module.Plugin.__dict__ if key.startswith('on_')]
            for event_name in implemented_events:
                if event_name == 'on_message':
                    # Store message events in {trigger: [plugin]} format
                    if not instantiated_plugin.triggers:
                        if 'on_any_message' not in self.events.keys():
                            self.events['on_any_message'] = []
                        self.events['on_any_message'].append(instantiated_plugin)
                    else:
                        if 'on_message' not in self.events.keys():
                            self.events['on_message'] = {}
                        for trigger in instantiated_plugin.triggers:
                            trigger = trigger.lower()
                            if trigger not in self.events['on_message'].keys():
                                self.events['on_message'][trigger] = []
                            self.events['on_message'][trigger].append(instantiated_plugin)
                else:
                    if event_name not in self.events.keys():
                        self.events[event_name] = []
                    self.events[event_name].append(instantiated_plugin)
        self._logger.info('Instantiated')

    def reload(self):
        self._logger.info('Reloading plugins')
        # Invalidate instantiated plugins with exception of non-reloadable plugins
        non_reloadable_events = {}
        for event_type in self.events:
            if isinstance(self.events[event_type], list):
                for plugin in self.events[event_type]:
                    if not plugin.hotreload_friendly:
                        if event_type not in non_reloadable_events.keys():
                            non_reloadable_events[event_type] = []
                        non_reloadable_events[event_type].append(plugin)
            elif isinstance(self.events[event_type], dict):
                for trigger in self.events[event_type]:
                    for plugin in self.events[event_type][trigger]:
                        if not plugin.hotreload_friendly:
                            if event_type not in non_reloadable_events.keys():
                                non_reloadable_events[event_type] = {}
                            if trigger not in non_reloadable_events[event_type].keys():
                                non_reloadable_events[event_type][trigger] = []
                            non_reloadable_events[event_type][trigger].append(plugin)
        self.events = non_reloadable_events

        for plugin in self.modules:
            if not plugin.Plugin.hotreload_friendly:
                continue
            self._logger.debug(f'Reloading {plugin.Plugin.name}')
            importlib.reload(plugin)  # Reload module
        self.instantiate_plugins(reload=True)

        self._logger.info('Reloaded')
