import argparse
from gettext import gettext as _
from typing import List

import discord

from core.client import Client
from utils.exceptions import ArgParserError, NoArguments


class ArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        # By default, ArgumentParser will exit the application if it errors, we don't want that
        raise ArgParserError(message)

    def __call__(self, *args, **kwargs):
        return self.parse_args(*args, **kwargs)

    def parse_args(self, args=None, namespace=None):
        # If there are no arguments, pass empty array, avoid passing None to prevent parsing sys.argv
        if not args:
            raise NoArguments
            args = []
        args, _ = self.parse_known_args(args, namespace)
        return args

    # Modify formatter to capitalize sections and be able to dynamically change prog
    def _get_formatter(self, prog=None):
        if not prog:
            prog = self.prog
        return self.formatter_class(prog=prog)

    def format_help(self, prog=None):
        formatter = self._get_formatter(prog)

        # usage
        formatter.add_usage(self.usage, self._actions, self._mutually_exclusive_groups, f"**{_('usage: ').capitalize()}**")

        # description
        formatter.add_text(self.description)

        # positionals, optionals and user-defined groups
        for action_group in self._action_groups:
            formatter.start_section(f'**{action_group.title.capitalize()}**')
            formatter.add_text(action_group.description)
            formatter.add_arguments(action_group._group_actions)
            formatter.end_section()

        # epilog
        formatter.add_text(self.epilog)

        # determine help from format above
        return formatter.format_help()


class BasePlugin:
    name = ''
    description = ''
    permissions = ()  # Required permission group(s)
    triggers = ()  # Words that trigger on_message() execution, if not set then executes on every message
    args = {}  # argparse configuration
    servers = ()  # Servers to run the plugin in
    hotreload_friendly = True
    disabled = False

    def __init__(self, bot_instance: Client):
        self.bot = bot_instance
        self.logger = bot_instance.log.get_logger(self.name)

        # Initialize argparse
        if self.args:
            self.argparser = ArgumentParser(description=self.description, add_help=False)
            for name, params in self.args.items():
                self.argparser.add_argument(name, **params)

    async def on_ready(self):
        pass

    async def on_any_message(self, message: discord.Message):
        pass

    async def on_message(self, message: discord.Message, trigger: str, args: List[str]):
        """
        :param message: Message the plugin was called with
        :param trigger: Trigger the plugin was called with
        :param args: Arguments the plugin was called with, split into a list
        """
        pass

    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        pass

    async def on_member_join(self, member: discord.Member):
        pass

    async def on_member_update(self, before: discord.Member, after: discord.Member):
        pass

    async def on_member_remove(self, member: discord.Member):
        pass

    async def on_guild_update(self, before: discord.Guild, after: discord.Guild):
        pass

    async def on_guild_role_update(self, before: discord.Role, after: discord.Role):
        pass
