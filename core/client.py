import re
from typing import Union

import discord
from discord.gateway import DiscordWebSocket

import utils
from core.database import Database
from core.influx import Influx
from core.logger import Logger
from core.plugin_manager import PluginManager
from utils.exceptions import ArgParserError, NoArguments


class Client(discord.Client):
    # Get list of Discord permissions
    Discord_Permissions = [prop
                           for prop, value in vars(discord.Permissions).items()
                           if isinstance(value, property)]

    def __init__(self, config):
        super(Client, self).__init__()
        self.config = config
        self.log = Logger(config['Logging'])
        self._logger = self.log.get_logger('Client')
        self._logger.info('Starting up')

        self.prefix = config['Client']['prefix']
        self.db = Database(self)
        self.plugin_manager = PluginManager(self)
        self.influx = Influx(self)

    def check_permissions(self, user: Union[discord.Member, discord.User], permitted_groups: list) -> bool:
        if not permitted_groups:
            return True

        permissions = self.config['Permissions']
        # Check against permission groups in config
        allowed_groups = list(filter(
            lambda g: g if g not in self.Discord_Permissions else None,
            permitted_groups
        ))
        for group in allowed_groups:
            # Check user IDs
            if 'users' in permissions[group].keys():
                if user.id in permissions[group]['users']:
                    return True
            # Check role IDs
            if 'roles' in permissions[group].keys():
                for role in user.roles:
                    if role.id in permissions[group]['roles']:
                        return True

        # Check user permissions
        allowed_permissions = list(filter(
            lambda p: p if p in self.Discord_Permissions else None,
            permitted_groups))
        for permission in allowed_permissions:
            if getattr(user.guild_permissions, permission, False):
                return True

        return False

    #  _____                 _         _ _                 _       _
    # | ____|_   _____ _ __ | |_    __| (_)___ _ __   __ _| |_ ___| |__   ___ _ __ ___
    # |  _| \ \ / / _ \ '_ \| __|  / _` | / __| '_ \ / _` | __/ __| '_ \ / _ \ '__/ __|
    # | |___ \ V /  __/ | | | |_  | (_| | \__ \ |_) | (_| | || (__| | | |  __/ |  \__ \
    # |_____| \_/ \___|_| |_|\__|  \__,_|_|___/ .__/ \__,_|\__\___|_| |_|\___|_|  |___/
    #                                         |_|

    async def on_connect(self):
        self._logger.info('Established connection to Discord')

    async def on_ready(self):
        self._logger.info(f'Logged in as {self.user.name}#{self.user.discriminator} with user ID {self.user.id}')
        for plugin in self.plugin_manager.events.get('on_ready', []):
            # Server check, if bot is not in any of the guilds, skip the event execution for this plugin
            if plugin.servers and not any([self.get_guild(server_id) for server_id in plugin.servers]):
                continue
            await plugin.on_ready()

    async def on_socket_response(self, msg: dict):
        # Latency monitoring
        if msg.get('op') == DiscordWebSocket.HEARTBEAT_ACK:
            await self.influx.write({
                'measurement': 'latency',
                'fields': {'ms': self.latency * 1000}
            })

    # Not an actual event, called from `on_message`
    async def on_any_message(self, message):
        for plugin in self.plugin_manager.events.get('on_any_message'):
            # If plugin is server-restricted, do a check
            if plugin.servers and message.guild.id not in plugin.servers:
                continue
            # Execute plugin
            await plugin.on_any_message(message)

    @utils.catch_and_respond(discord.errors.Forbidden, dm=True, reraise=True, fields=[('⚠ Permission error', '{0}')], colour=utils.Colours.warning)
    async def on_message(self, message: discord.Message):
        await self.influx.write({
            'measurement': 'events',
            'fields': {'event': 'on_message'},
            'tags': {'event': 'on_message'}
        })

        # Return if there are no plugins with `on_message` implemented
        implemented_events = self.plugin_manager.events.keys()
        if 'on_message' not in implemented_events or 'on_any_message' not in implemented_events:
            return
        # Ignore direct messages and group channels
        if isinstance(message.channel, (discord.DMChannel, discord.GroupChannel)):
            return
        # Ignore bot users
        if message.author.bot:
            return

        await self.on_any_message(message)  # Execute on_any_message() plugins

        # Parse the message
        matched_plugins, trigger, text, args = [], None, None, None
        match = re.match(rf'{self.prefix}([^\s]+)\s?(.+)?', message.content, flags=re.DOTALL)
        if match:
            # Match the trigger with plugins
            trigger, text = match.groups()
            plugins = self.plugin_manager.events['on_message'].get(trigger.lower())
            if plugins:
                matched_plugins += plugins
            if text:
                args = []
                for line in text.splitlines():
                    args.extend(line.split())
                    args.append('\n')
                args.pop()

        if not matched_plugins:
            return

        await self.influx.write({
            'measurement': 'events',
            'fields': {'event': 'on_command'},
            'tags': {'event': 'on_command'},
        })

        # Execute the matched plugins
        for plugin in matched_plugins:
            # If plugin is server-restricted, do a check
            if plugin.servers and message.guild.id not in plugin.servers:
                continue
            if trigger and plugin.triggers:
                self._logger.info(
                    f"{message.author.name}#{message.author.discriminator} ({message.author.id}) "
                    f"on {message.guild.name} ({message.guild.id}) "
                    f"used command {plugin.name} with trigger {trigger} and arguments: {repr(text)}")
            # Permission check
            if plugin.permissions:
                if not self.check_permissions(message.author, plugin.permissions):
                    continue
            # Execute plugin
            try:
                await plugin.on_message(message, trigger, args)
            except ArgParserError as error:
                response = utils.create_embed(fields=[('⚠ Argument error', error.message)], colour=utils.Colours.warning)
                await message.channel.send(embed=response)
            except NoArguments:
                plugin_help = plugin.argparser.format_help(self.prefix + trigger)
                response = utils.create_embed(fields=[(f'📃 {plugin.name}', plugin_help, False)],
                                              colour=utils.Colours.info)
                await message.channel.send(embed=response)

    async def on_message_edit(self, before: discord.Message, after: discord.Message):
        if after.author.bot:
            return

        for plugin in self.plugin_manager.events.get('on_message_edit', []):
            # If plugin is server-restricted, do a check
            if plugin.servers and after.guild.id not in plugin.servers:
                continue
            await plugin.on_message_edit(before, after)

    async def on_member_join(self, member: discord.Member):
        self._logger.info(f"{member.name}#{member.discriminator} ({member.id}) has joined {member.guild.name} ({member.guild.id})")
        for plugin in self.plugin_manager.events.get('on_member_join', []):
            # If plugin is server-restricted, do a check
            if plugin.servers and member.guild.id not in plugin.servers:
                continue
            await plugin.on_member_join(member)

    async def on_member_remove(self, member: discord.Member):
        self._logger.info(f"{member.name}#{member.discriminator} ({member.id}) has left {member.guild.name} ({member.guild.id})")
        for plugin in self.plugin_manager.events.get('on_member_remove', []):
            # If plugin is server-restricted, do a check
            if plugin.servers and member.guild.id not in plugin.servers:
                continue
            await plugin.on_member_remove(member)

    async def on_guild_update(self, before: discord.Guild, after: discord.Guild):
        for plugin in self.plugin_manager.events.get('on_member_remove', []):
            # If plugin is server-restricted, do a check
            if plugin.servers and before.id not in plugin.servers:
                continue
            await plugin.on_guild_update(before, after)

    async def on_guild_role_update(self, before: discord.Role, after: discord.Role):
        for plugin in self.plugin_manager.events.get('on_guild_role_update', []):
            # If plugin is server-restricted, do a check
            if plugin.servers and before.id not in plugin.servers:
                continue
            await plugin.on_guild_role_update(before, after)
